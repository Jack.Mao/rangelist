# RangeList

A RangeList implement to solve range union and except problem

## Installation

##### From [RubyGems](https://rubygems.org)

    $ gem install range_list_jack

##### From source code

Get Code from Gitlab:

	$ git clone https://gitlab.com/Jack.Mao/rangelist.git

Build and Install with Bundle:

    $ cd rangelist
    $ bundle install
    $ bundle exec rake install

## Uninstall

	$ gem uninstall range_list_jack

## Examples

```ruby
require range_list

rl = RangeList::RangeList.new
rl.add([1, 5])
rl.print
# Should display: [1, 5)
rl.add([10, 20])
rl.print
# Should display: [1, 5) [10, 20)
rl.add([20, 20])
rl.print
# // Should display: [1, 5) [10, 20)
rl.add([20, 21])
rl.print
# Should display: [1, 5) [10, 21)
rl.add([2, 4])
rl.print
# Should display: [1, 5) [10, 21)
rl.add([3, 8])
rl.print
# Should display: [1, 8) [10, 21)
rl.remove([10, 10])
rl.print
# Should display: [1, 8) [10, 21)
rl.remove([10, 11])
rl.print
# Should display: [1, 8) [11, 21)
rl.remove([15, 17])
rl.print
# Should display: [1, 8) [11, 15) [17, 21)
rl.remove([3, 19])
rl.print
# Should display: [1, 3) [19, 21)
```

