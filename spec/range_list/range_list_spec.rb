RSpec.describe RangeList do
  it "has a version number" do
    expect(RangeList::VERSION).not_to be nil
  end

  it "should display expected string" do
    rl = RangeList::RangeList.new

    rl.add([1, 5])
    expect(rl.to_s).to eq '[1, 5)'

    rl.add([10, 20])
    expect(rl.to_s).to eq '[1, 5) [10, 20)'

    rl.add([20, 20])
    expect(rl.to_s).to eq '[1, 5) [10, 20)'

    rl.add([20, 21])
    expect(rl.to_s).to eq '[1, 5) [10, 21)'

    rl.add([2, 4])
    expect(rl.to_s).to eq '[1, 5) [10, 21)'

    rl.add([3, 8])
    expect(rl.to_s).to eq '[1, 8) [10, 21)'

    rl.remove([10, 10])
    expect(rl.to_s).to eq '[1, 8) [10, 21)'

    rl.remove([10, 11])
    expect(rl.to_s).to eq '[1, 8) [11, 21)'

    rl.remove([15, 17])
    expect(rl.to_s).to eq '[1, 8) [11, 15) [17, 21)'

    rl.remove([3, 19])
    expect(rl.to_s).to eq '[1, 3) [19, 21)'
  end
end
