require "range_list/version"
require "range_list/range_list"

module RangeList
  class Error < StandardError; end
end
