# Your code goes here...
# A pair of integers define a range, for example: [1, 5).
# This range includes integers: 1, 2, 3, and 4.
# A range list is an aggregate of these ranges: [1, 5), [10, 11), [100, 201)
module RangeList
  class RangeList
    def initialize
      @all = Set.new
    end

    def add(range)
      @all += Set.new(range[0]...range[1])
    end

    def remove(range)
      @all -= Set.new(range[0]...range[1])
    end

    def print
      puts to_s
    end

    def to_s
      ret, before = "", 0
      a = @all.sort
      # append a number greater than max element to avoid last range problem
      a.append(a[-1]+3)
      for i in (1...a.size)
        if a[i] - a[i-1] > 1
          ret += "#{ret.empty? ? '':' '}[#{a[before]}, #{a[i-1] + 1})"
          before = i
        end
      end
      return ret
    end
  end
end

